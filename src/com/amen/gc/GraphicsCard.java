package com.amen.gc;

public class GraphicsCard {
	private int processingPower;
	private GraphicsStream stream; // przypisane ustawienia / stream

	public GraphicsCard(int processingPower, GraphicsStream stream) {
		super();
		this.processingPower = processingPower;
		this.stream = stream;
	}

	public void assignStream(GraphicsStream stream) {
		this.stream = stream;
	}

	public boolean checkStream() {
		double potrzebna_moc = stream.getResolution().getHeight() * stream.getResolution().getWidth()
				* stream.getColors().getColors();

		double formatResolution = (double) stream.getResolution().getWidth() / stream.getResolution().getHeight();
		double format = (double) stream.getFormat().getWidth() / stream.getFormat().getHeight();
		if (potrzebna_moc < processingPower) {
			if (formatResolution < format) {
				System.out.println("Komunikat");
				return true;
			} else if (formatResolution == format) {
				return true;
			}
		}
		return false;
	}

	public CABLE whichCable() {
		double potrzebna_moc = stream.getResolution().getHeight() * stream.getResolution().getWidth()
				* stream.getColors().getColors();
		if (potrzebna_moc > 16785408) {
			return CABLE.HDMI;
		} else if (potrzebna_moc > 7680000) {
			return CABLE.DVI;
		} else if (potrzebna_moc > 480000) {
			return CABLE.VGA;
		}
		return null;
	}
}
