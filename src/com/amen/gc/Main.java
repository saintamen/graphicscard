package com.amen.gc;

public class Main {

	public static void main(String[] args) {
		GraphicsStream st = GraphicsStreamFactory.createFullHD();
		GraphicsCard card = new GraphicsCard(2012030123, st);
		
		card.checkStream();
	}

}
