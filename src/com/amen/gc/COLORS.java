package com.amen.gc;

public enum COLORS {
	B32(32), B16(16), B8(8), MONO(2);

	private int colors;

	private COLORS(int colors) {
		this.colors = colors;
	}

	public int getColors() {
		return colors;
	}

}
