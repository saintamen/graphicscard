package com.amen.gc;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GraphicsTests {

	GraphicsCard karta;
	
	@Before 
	public void prepare(){
		// 1920 *
		karta = new GraphicsCard(66355000, GraphicsStreamFactory.createFullHD());
	}

	@Test
	public void test1(){
		Assert.assertEquals(false, karta.checkStream());
	}
	
	@Test
	public void test2(){
		// 
		karta.assignStream(GraphicsStreamFactory.createHDReady());
		Assert.assertEquals(false, karta.checkStream());
	}
	
	@Test
	public void test3(){
		Assert.assertEquals(CABLE.HDMI, karta.whichCable());
	}
}
