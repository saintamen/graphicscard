package com.amen.gc;

public class GraphicsStreamFactory {

	public static GraphicsStream createFullHD() {
		return new GraphicsStream(RESOLUTION.R1920x1080, COLORS.B32, FORMAT.F16x9);
	}
	
	public static GraphicsStream createHDReady() {
		return new GraphicsStream(RESOLUTION.R1366x768, COLORS.B32, FORMAT.F16x9);
	}
}
