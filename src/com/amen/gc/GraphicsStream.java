package com.amen.gc;

public class GraphicsStream {
	private RESOLUTION resolution;
	private COLORS colors;
	private FORMAT format;

	public GraphicsStream(RESOLUTION resolution, COLORS colors, FORMAT format) {
		super();
		this.resolution = resolution;
		this.colors = colors;
		this.format = format;
	}

	public RESOLUTION getResolution() {
		return resolution;
	}
	
	public COLORS getColors() {
		return colors;
	}
	
	public FORMAT getFormat() {
		return format;
	}
}
