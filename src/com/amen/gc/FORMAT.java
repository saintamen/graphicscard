package com.amen.gc;

public enum FORMAT {

	F16x10(16, 10), F16x9(16, 9), F4x3(4, 3), F3x2(3, 2);

	private int w;
	private int h;

	private FORMAT(int w, int h) {
		this.w = w;
		this.h = h;
	}

	public int getHeight() {
		return h;
	}

	public int getWidth() {
		return w;
	}

}
